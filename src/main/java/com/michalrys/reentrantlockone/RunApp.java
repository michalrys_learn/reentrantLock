package com.michalrys.reentrantlockone;

public class RunApp {
    public static final int UPDATER_CALLS = 5;
    public static final int CALCULATOR_CALLS = 5;

    public static void main(String[] args) {
        System.out.println("ReentrantLock - lock only for adding sth");
        Context context = new Context();
        System.out.println(context);

        context.setLockManager();

        for (int id = 1; id <= UPDATER_CALLS; id++) {
            new Updater(context, "Upd-" + id).updateValue();
        }
        for (int id = 1; id <= CALCULATOR_CALLS; id++) {
            new Calculator(context, "Calc-" + id).useItSomewhereElse();
        }
    }
}

package com.michalrys.reentrantlockone;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;

public class Updater {
    public static final long EXECUTION_TIME = 10L;
    public static final int HOW_MANY_TIME_METHOD_WILL_BE_CALLED = RunApp.UPDATER_CALLS; //previous value was 5;
    // 1 method takes 10ms, so when we call n times then time is (n-1) * 10ms
    public static final long WAIT_FOR_LOCK = (HOW_MANY_TIME_METHOD_WILL_BE_CALLED - 1) * EXECUTION_TIME;

    private final Context context;
    private final String name;

    public Updater(Context context, String name) {
        this.context = context;
        this.name = name;
    }

    public void updateValue() {
        //this method takes 10ms
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                String time = LocalTime.now().format(DateTimeFormatter.ofPattern("HH:mm.n"));
                System.out.printf("\tcall %s, time=%s\n", name, time);
                Lock lock = context.getLockManager();
                try {
                    if (lock.tryLock(WAIT_FOR_LOCK, TimeUnit.MILLISECONDS)) {
                        try {
                            updateCounterInContext();
                        } finally {
                            lock.unlock();
                        }
                    } else {
                        time = LocalTime.now().format(DateTimeFormatter.ofPattern("HH:mm.n"));
                        System.out.printf("\t%s: It was too long to wait, so I break it. time=%s\n", name, time);
                    }
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }

            private void updateCounterInContext() {
                LocalTime start = LocalTime.now();
                String timeStart = start.format(DateTimeFormatter.ofPattern("HH:mm.n"));
                System.out.printf("%s) START Current context.counter = %d,    time=%s\n", name, context.getCounter(), timeStart);
                try {
                    Thread.sleep(8L);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
                int counter = context.getCounter();
                counter++;
                context.setCounter(counter);
                LocalTime end = LocalTime.now();
                String timeEnd = end.format(DateTimeFormatter.ofPattern("HH:mm.n"));
                System.out.printf("%s) END context.counter was updated to %d, time=%s\n", name, context.getCounter(), timeEnd);
                LocalTime executionTime = end.minusHours(start.getHour()).minusMinutes(start.getMinute()).minusSeconds(start.getSecond()).minusNanos(start.getNano());
                System.out.printf("%s) %s\n", name, executionTime);
            }
        });
        t.start();
    }
}

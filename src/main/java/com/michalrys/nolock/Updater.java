package com.michalrys.nolock;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

public class Updater {
    private final Context context;
    private final String name;

    public Updater(Context context, String name) {
        this.context = context;
        this.name = name;
    }

    public void updateValue() {
        //this method takes 10ms
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                String time = LocalTime.now().format(DateTimeFormatter.ofPattern("HH:mm.n"));
                System.out.printf("\tcall %s, time=%s\n", name, time);
                updateCounterInContext();
            }

            private void updateCounterInContext() {
                LocalTime start = LocalTime.now();
                String timeStart = start.format(DateTimeFormatter.ofPattern("HH:mm.n"));
                System.out.printf("%s) START Current context.counter = %d,    time=%s\n", name, context.getCounter(), timeStart);

                try {
                    Thread.sleep(8L);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }

                int counter = context.getCounter();
                counter++;
                context.setCounter(counter);
                LocalTime end = LocalTime.now();
                String timeEnd = end.format(DateTimeFormatter.ofPattern("HH:mm.n"));
                System.out.printf("%s) END context.counter was updated to %d, time=%s\n", name, context.getCounter(), timeEnd);
                LocalTime executionTime = end.minusHours(start.getHour()).minusMinutes(start.getMinute()).minusSeconds(start.getSecond()).minusNanos(start.getNano());
                System.out.printf("%s) %s\n", name, executionTime);
            }
        });
        t.start();
    }
}

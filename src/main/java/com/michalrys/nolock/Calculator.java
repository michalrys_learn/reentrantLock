package com.michalrys.nolock;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

public class Calculator {
    private final Context context;
    private final String name;
    private int calculation;

    public Calculator(Context context, String name) {
        this.context = context;
        this.name = name;
    }

    public void useItSomewhereElse() {
        //this method takes 30ms
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                String time = LocalTime.now().format(DateTimeFormatter.ofPattern("HH:mm.n"));
                System.out.printf("\t\tcall %s, time=%s\n", name, time);
                carryOnCalculations();
            }

            private void carryOnCalculations() {
                try {
                    LocalTime start = LocalTime.now();
                    String timeStart = start.format(DateTimeFormatter.ofPattern("HH:mm.n"));
                    System.out.printf("%s) START Current context.counter = %d, time=%s\n", name, context.getCounter(), timeStart);
                    int counter = context.getCounter();
                    calculation = 10;
                    Thread.sleep(5);
                    calculation += counter;
                    Thread.sleep(5);
                    calculation += 100;
                    Thread.sleep(5);
                    calculation += 1000;
                    Thread.sleep(5);
                    calculation += 1000;
                    Thread.sleep(5);
                    LocalTime end = LocalTime.now();
                    String timeEnd = end.format(DateTimeFormatter.ofPattern("HH:mm.n"));
                    System.out.printf("%s) END Current context.counter = %d,   time=%s\n", name, context.getCounter(), timeEnd);
                    LocalTime executionTime = end.minusHours(start.getHour()).minusMinutes(start.getMinute()).minusSeconds(start.getSecond()).minusNanos(start.getNano());
                    System.out.printf("%s) %s\n", name, executionTime);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
        });
        t.start();
    }
}

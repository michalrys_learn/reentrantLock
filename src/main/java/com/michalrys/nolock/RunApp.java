package com.michalrys.nolock;

public class RunApp {
    public static void main(String[] args) {
        System.out.println("No lock");
        Context context = new Context();
        System.out.println(context);

        for (int id = 1; id <= 5; id++) {
            new Updater(context, "Upd-" + id).updateValue();
            new Calculator(context, "Calc-" + id).useItSomewhereElse();
        }
//        verification(context);
    }

    private static void verification(Context context) {
        Updater updater_1 = new Updater(context, "Upd-1");
        Updater updater_2 = new Updater(context, "Upd-2");
        Updater updater_3 = new Updater(context, "Upd-3");
        Updater updater_4 = new Updater(context, "Upd-4");
        Updater updater_5 = new Updater(context, "Upd-5");
        Calculator calc_1 = new Calculator(context, "Calc-1"); // 1 call takes 30ms
        Calculator calc_2 = new Calculator(context, "Calc-2");
        Calculator calc_3 = new Calculator(context, "Calc-3");
        Calculator calc_4 = new Calculator(context, "Calc-4"); // so for 4 calls 3x30ms shall be set to wait
        Calculator calc_5 = new Calculator(context, "Calc-5"); // so for 5 calls 4x30ms shall be set to wait

        calc_1.useItSomewhereElse();
        calc_2.useItSomewhereElse();
        updater_1.updateValue();
        updater_2.updateValue();
        calc_3.useItSomewhereElse();
        updater_3.updateValue();
        updater_4.updateValue();
        calc_4.useItSomewhereElse();
        updater_5.updateValue();
        calc_5.useItSomewhereElse();
    }
}

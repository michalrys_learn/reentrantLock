package com.michalrys.reentrantlockboth;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Context {
    private int counter;
    private Lock lockManager;

    public Context() {
        this.counter = 0;
    }

    public int getCounter() {
        return counter;
    }

    public void setCounter(int counter) {
        this.counter = counter;
    }

    @Override
    public String toString() {
        return "Context{" +
                "counter=" + counter +
                '}';
    }

    public void setLockManager() {
        lockManager = new ReentrantLock();
    }

    public Lock getLockManager() {
        return lockManager;
    }
}

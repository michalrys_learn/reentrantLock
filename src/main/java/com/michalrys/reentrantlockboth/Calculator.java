package com.michalrys.reentrantlockboth;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;

public class Calculator {
    public static final long EXECUTION_TIME = 30L;
    public static int HOW_MANY_TIME_METHOD_WILL_BE_CALLED = RunApp.CALCULATOR_CALLS;  //previous value was 5
    // 1 method takes 30ms, so when we call n times then time is (n-1) * 30ms
    // it shall be increased by waiting for all calls to updater method
    public static final long WAIT_FOR_LOCK = (HOW_MANY_TIME_METHOD_WILL_BE_CALLED - 1) * EXECUTION_TIME
            + Updater.HOW_MANY_TIME_METHOD_WILL_BE_CALLED * Updater.EXECUTION_TIME;
    private final Context context;
    private final String name;
    private int calculation;

    public Calculator(Context context, String name) {
        this.context = context;
        this.name = name;
    }

    public void useItSomewhereElse() {
        //this method takes 30ms
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                String time = LocalTime.now().format(DateTimeFormatter.ofPattern("HH:mm.n"));
                System.out.printf("\t\tcall %s, time=%s\n", name, time);
                Lock lock = context.getLockManager();
                try {
                    if (lock.tryLock(WAIT_FOR_LOCK, TimeUnit.MILLISECONDS)) {
                        try {
                            carryOnCalculations();
                        } finally {
                            lock.unlock();
                        }
                    } else {
                        time = LocalTime.now().format(DateTimeFormatter.ofPattern("HH:mm.n"));
                        System.out.printf("\t\t%s) I cant wait %sms., so I break it. time=%s\n", name, WAIT_FOR_LOCK, time);
                    }
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }

            private void carryOnCalculations() {
                try {
                    LocalTime start = LocalTime.now();
                    String timeStart = start.format(DateTimeFormatter.ofPattern("HH:mm.n"));
                    System.out.printf("%s) START Current context.counter = %d, time=%s\n", name, context.getCounter(), timeStart);
                    int counter = context.getCounter();
                    calculation = 10;
                    Thread.sleep(5);
                    calculation += counter;
                    Thread.sleep(5);
                    calculation += 100;
                    Thread.sleep(5);
                    calculation += 1000;
                    Thread.sleep(5);
                    calculation += 1000;
                    Thread.sleep(5);
                    LocalTime end = LocalTime.now();
                    String timeEnd = end.format(DateTimeFormatter.ofPattern("HH:mm.n"));
                    System.out.printf("%s) END Current context.counter = %d,   time=%s\n", name, context.getCounter(), timeEnd);
                    LocalTime executionTime = end.minusHours(start.getHour()).minusMinutes(start.getMinute()).minusSeconds(start.getSecond()).minusNanos(start.getNano());
                    System.out.printf("%s) %s\n", name, executionTime);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
        });
        t.start();
    }
}

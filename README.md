## Aim
In this repo I present simple usecases for tryLock method taken from ReentrantLock. 

Main flow:
- `Context` shares `counter`
- `Updater.updateCounterInContext()` updates content of `context.counter` asynchronously.
- `Calculator.carryOnCalculations()` reads content of `context.counter` asynchronously and uses it for some internal calculations only.
- `RunApp` initiate `context.counter` and runs `Updater` and `Calculator` several times.

Variants:

1) `com.michalrys.nolock.RunApp`
   - no locks.

2) `com.michalrys.reentrantlockboth.RunApp`
   - locks for reading and updating content of `context.counter`.
   
3) `com.michalrys.reentrantlockone.RunApp`
   - locks for updating content of `context.counter` only.

## Locking mechanism
We could use synchronized keyword or Lock for blocking threads in Java. See comparison in following [article](https://anytech.medium.com/java-synchronized-vs-reentrantlock-which-lock-should-i-use-really-0cd8bd7cd3e5).
